<?php

namespace Drupal\uw_bibcite_reference;

use Drupal\Core\Cache\CacheableMetadata;
use Drupal\Core\Config\ConfigFactoryOverrideInterface;
use Drupal\Core\Config\StorageInterface;

/**
 * Provides pathauto overrides for the configuration factory.
 */
class ConfigPathautoOverrides implements ConfigFactoryOverrideInterface {

  /**
   * {@inheritdoc}
   */
  public function loadOverrides($names) {

    $overrides = [];

    if (in_array('pathauto.settings', $names)) {
      $overrides['pathauto.settings'] = [
        'enabled_entity_types' => [
          'bibcite_contributor',
          'bibcite_keyword',
          'bibcite_reference',
        ],
      ];
    }

    return $overrides;
  }

  /**
   * {@inheritdoc}
   */
  public function getCacheSuffix() {
    return 'ConfigExampleOverrider';
  }

  /**
   * {@inheritdoc}
   */
  public function getCacheableMetadata($name) {
    return new CacheableMetadata();
  }

  /**
   * {@inheritdoc}
   */
  public function createConfigObject($name, $collection = StorageInterface::DEFAULT_COLLECTION) {
    return NULL;
  }

}
