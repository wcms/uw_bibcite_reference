<?php

namespace Drupal\uw_bibcite_reference\Form;

use Drupal\Component\Utility\Html;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;

/**
 * Class PublicationSearchForm.
 *
 * Form for searching all references.
 */
class PublicationSearchForm extends FormBase {

  /**
   * Entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * Class constructor.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entityTypeManager
   *   The entity type manager.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public function __construct(EntityTypeManagerInterface $entityTypeManager) {
    $this->entityTypeManager = $entityTypeManager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    // Instantiates this form class.
    return new static(
      $container->get('entity_type.manager'),
    );
  }

  /**
   * Returns a unique string identifying the form.
   *
   * The returned ID should be a unique string that can be a valid PHP function
   * name, since it's used in hook implementation names such as
   * hook_form_FORM_ID_alter().
   *
   * @return string
   *   The unique string identifying the form.
   */
  public function getFormId(): string {
    return 'publication_search_form';
  }

  /**
   * Form constructor.
   *
   * @param array $form
   *   An associative array containing the structure of the form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   * @param array $search_settings
   *   The settings for the search.
   *
   * @return array
   *   The form structure.
   */
  public function buildForm(array $form, FormStateInterface $form_state, array $search_settings = NULL): array {

    // Add the unique id.
    $form['#attributes']['id'] = 'publication-search-form-' . $search_settings['uuid'];

    // Set the search settings in the form state.
    $form_state->set('search_settings', $search_settings);

    // The search input.
    $form['search_input'] = [
      '#type' => 'textfield',
      '#placeholder' => $search_settings['search_placeholder'] ?? NULL,
      '#description' => $search_settings['search_description'] ?? NULL,
      '#required' => TRUE,
      '#required_error' => $this->t('This field is required.'),
    ];

    // Set the id for the search input.
    $form['search_input']['#attributes']['id'] = 'edit-search-input-' . $search_settings['uuid'];

    // Add a submit button that handles the submission of the form.
    $form['submit'] = [
      '#type' => 'submit',
      '#attributes' => ['aria-label' => 'search'],
    ];

    return $form;
  }

  /**
   * Form submission handler.
   *
   * @param array $form
   *   An associative array containing the structure of the form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {

    // Get the values of the form state.
    $values = $form_state->getValues();

    // Get the search settings from the form state.
    $search_settings = $form_state->get('search_settings');

    // Set the parameter count and parameter string to default.
    $parameter_count = 0;
    $parameters = '';

    // Get the keywords if using specific keywords.
    if ($search_settings['keyword'] == 'specific') {

      // Step through each of the keywords and get the name.
      foreach ($search_settings['keywords'] as $keyword) {

        // If there is a keyword, get the name.
        if ($keyword) {

          // Load the keyword entity.
          $keyw = $this->entityTypeManager->getStorage('bibcite_keyword')->load($keyword);

          // Add the keyword name.
          $keywords[] = $keyw->getName();
        }
      }

      // If there are keywords, add to parameter string.
      if (count($keywords) > 0) {

        // If there are other parameters add the &.
        if ($parameter_count > 0) {
          $parameters .= '&';
        }

        // Add the keywords to the parameter string.
        $parameters .= 'keywords=' . implode('%2C', $keywords);

        // Increment to parameter count, so we know to add the &.
        $parameter_count++;
      }
    }

    // Get the types if using specific types.
    if ($search_settings['type'] == 'specific') {

      // Step through each of the types and get the name.
      foreach ($search_settings['types'] as $type) {

        // If there is a type, then add to the parameter string.
        // In order to use the view we need types to be in the
        // parameter string as type[]=.
        if ($type) {

          // If there are more than one parameter, add the &.
          if ($parameter_count > 0) {
            $parameters .= '&';
          }

          // Add the type to the parameter string.
          $parameters .= 'type[]=' . $type;

          // Increment to parameter count, so we know to add the &.
          $parameter_count++;
        }
      }
    }

    // If there is year, add it to the parameter string.
    if ($search_settings['year']) {

      // If there are more than one parameter, add the &.
      if ($parameter_count > 0) {
        $parameters .= '&';
      }

      // Add the year to the parameter string.
      $parameters .= 'year=' . $search_settings['year'];
    }

    // If there is a search input, add it to the parameter string.
    if ($values['search_input']) {

      // If there are more than one parameter, add the &.
      if ($parameter_count > 0) {
        $parameters .= '&';
      }

      // Add the search to the parameter string.
      $parameters .= 'search=' . Html::escape($values['search_input']);

      // Increment the parameter count, to ensure that we get correct &.
      $parameter_count++;
    }

    // If there is a sort by setting, add it to the parameter string.
    if ($search_settings['sort_by']) {

      // If there are more than one parameter, add the &.
      if ($parameter_count > 0) {
        $parameters .= '&';
      }

      // Add the sort by to the parameter string.
      $parameters .= 'sort_by=' . $search_settings['sort_by'];

      // Increment the parameter count, to ensure that we get correct &.
      $parameter_count++;
    }

    // If there is a sort order setting, add it to the parameter string.
    if ($search_settings['sort_order']) {

      // If there are more than one parameter, add the &.
      if ($parameter_count > 0) {
        $parameters .= '&';
      }

      // Add the sort order to the parameter string.
      $parameters .= 'sort_order=' . $search_settings['sort_order'];

      // Increment the parameter count, to ensure that we get correct &.
      $parameter_count++;
    }

    // Get the URL to the reference list view.
    $url = Url::fromRoute('view.bibcite_reference.bibcite_reference', [], ['absolute' => TRUE])->toString();

    // Add the parameters to the view.
    $url .= '?' . $parameters;

    // Set the redirection to the view.
    $response = new RedirectResponse($url);
    $response->send();
  }

}
